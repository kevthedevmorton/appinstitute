<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 24/02/2016
 * Time: 06:39
 */
namespace core;

class Route {

    private static $routes = [];

    /**
     * Route constructor.
     */
    private function __construct() {

    }

    /**
     *
     */
    private function __clone() {

    }

    /**
     * @param $pattern
     * @param $callback
     */
    public static function route($pattern, $callback) {

        $pattern = '/^\/' . str_replace('/', '\/', $pattern) . '(\?|$)/';
        self::$routes[$pattern] = $callback;
    }

    /**
     * @param string $url
     * @return mixed
     */
    public static function execute($url = "") {

        if (empty($url)) {
            $url = $_SERVER['REQUEST_URI'];
        }
        foreach (self::$routes as $pattern => $callback) {
            if (preg_match($pattern, $url, $params)) {
                array_shift($params);
                return call_user_func_array($callback, array_values($params));
            }
        }
    }

}
