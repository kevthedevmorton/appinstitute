<?php

/**
 * @param $className
 */
function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName = '';
    $appDirectory = APP_PATH.'\\app\\';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName = $appDirectory.str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR .$className .'.php';
    }

    /** @var TYPE_NAME $fileName */
    require $fileName;
}

spl_autoload_register('autoload');