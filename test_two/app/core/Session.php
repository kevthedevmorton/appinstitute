<?php

/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 24/02/2016
 * Time: 05:37
 */
namespace core;

class Session
{
    protected $session;
    protected $cookies;
    /**
     * Session constructor.
     */
    public function __construct()
    {
        session_start();
        $this->session = $_SESSION;
        $this->cookies = $_COOKIE;
        return $this;
    }

    function setSession($key,$value){
        $_SESSION[$key]=$value;
        $this->session = $_SESSION;
    }
    function setCookie($key,$value){
        setcookie($key,$value);
        $this->cookies = $_COOKIE;
    }

}