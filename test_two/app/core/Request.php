<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 24/02/2016
 * Time: 06:01
 */
namespace core;

class Request
{
    protected $get;
    protected $request;
    protected $post;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->get = $_GET;
        $this->request = $_REQUEST;
        $this->post = $_POST;
        return $this;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getGet($key)
    {
        if (!empty($this->get[$key])) {
            return $this->get[$key];
        }
        return null;
    }

    /**
     * @param mixed $get
     * @return $this
     */
    public function setGet($get)
    {
        $this->get = $get;
        return $this;
    }

    /**
     * @param $key
     * @return array
     */
    public function getRequest($key)
    {
        if (!empty($this->request[$key])) {
            return $this->request[$key];
        }
        return null;
    }

    /**
     * @param mixed $request
     * @return $this
     */
    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getPost($key)
    {
        if (!empty($this->post[$key])) {
            return $this->post[$key];
        }
        return null;
    }

    /**
     * @param array $post
     * @return $this
     */
    public function setPost($post)
    {
        $this->post = $post;
        return $this;
    }

}