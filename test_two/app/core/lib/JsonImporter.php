<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 24/02/2016
 * Time: 08:07
 */
namespace core\lib ;
trait JsonImporter{
    static function import($dir , $type){
        $json = file_get_contents($dir);
        $jsonIterator = json_decode($json, TRUE);
        return self::createInstances($jsonIterator , $type);
    }
    public static function createInstances($iterator, $type) {

        $reflectionClass= new \ReflectionClass($type);
        $ret = [];
        foreach ($iterator as  $row) {

            array_push($ret, $reflectionClass->newInstanceArgs((array)$row));
        }
        return $ret;
    }
}