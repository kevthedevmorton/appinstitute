<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 24/02/2016
 * Time: 04:01
 */
namespace core;
class Response
{
    protected $header;
    protected $content;
    protected $layout;
    protected $isCached;

    /**
     * Response constructor.
     */
    public function __construct()
    {
        return $this;
    }

    /**
     *
     */
    public function render()
    {
        if(!empty($this->header)){
            header("Content-Type: {$this->header}");
        }

        echo $this->getContent();

    }

    /**
     * @param $url
     */
    public function location($url)
    {
       return header("Location: {$url}",true,307);
    }
    /**
     * @return mixed
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @param mixed $header
     * @return Response
     */
    public function setHeader($header)
    {
        $this->header = $header;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param mixed $layout
     * @return $this
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsCached()
    {
        return $this->isCached;
    }

    /**
     * @param mixed $isCached
     * @return $this
     */
    public function setIsCached($isCached)
    {
        $this->isCached = $isCached;
        return $this;
    }

}