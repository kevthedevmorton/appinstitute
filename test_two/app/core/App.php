<?php

/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 24/02/2016
 * Time: 05:03
 */
namespace core;
abstract class App
{

    private $request;
    private $response;
    private $session;

    /**
     * App constructor.
     */
    public final function __construct()
    {
        $this->request = new Request();
        $this->response = new Response();
        $this->session = new Session();

    }

    /**
     * @return Request
     */
    public final function getRequest()
    {
        return $this->request;
    }

    /**
     * @return Session
     */
    public final function getSession()
    {
        return $this->session;
    }

    /**
     * @return Response
     */
    public final function getResponse()
    {
        return $this->response;
    }

}