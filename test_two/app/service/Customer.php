<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 24/02/2016
 * Time: 08:07
 */
namespace service;

use core\APP;
use core\lib\JsonImporter;
use entity\Location;

class Customer extends APP
{
    use JsonImporter;

    function getNearCustomers()
    {
        $customers = self::import(APP_PATH . "/data/customer_data.json", '\entity\Customer');
        $validCustomers = [];
        $lat = $this->getRequest()->getGet('lat');
        $lon = $this->getRequest()->getGet('lon');
        $distanceFrom = $this->getRequest()->getGet('distance');
        foreach ($customers as $customer) {
            //52.951458, -1.142332
            $distance = $customer->getLocation()->distanceFrom(new Location($lat,$lon));
            if ($distance < $distanceFrom) {
                $validCustomers[] = ['customer' => $customer->getName(), 'distance'=>$distance];
            }
        }
        usort($validCustomers, function($a, $b) {
            return $a['distance'] - $b['distance'];
        });
        $this->getResponse()
             ->setHeader('application/json')
             ->setContent(json_encode($validCustomers))
             ->render();

    }
}