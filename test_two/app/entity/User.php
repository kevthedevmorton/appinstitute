<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 23/02/2016
 * Time: 05:53
 */
namespace entity;

abstract class User
{
    protected $name;
    protected $location;

    /**
     * User constructor.
     * @param $name
     * @param $location
     */
    public function __construct($name ="", $location =null)
    {
        $this->setName($name);
        $this->setLocation(new Location($location['lat'],$location['lon']));
        return $this;
    }


    abstract function __clone();

    function getName()
    {
        return $this->name;
    }

    function setName($name)
    {
        $this->name = $name;
    }

    function getLocation()
    {
        return $this->location;
    }

    function setLocation(Location $location)
    {
        $this->location = $location;
    }


}
