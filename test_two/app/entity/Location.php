<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 23/02/2016
 * Time: 05:54
 */
namespace entity;

class Location
{
    protected $lat;
    protected $lon;

    /**
     * Location constructor.
     * @param $lat
     * @param $lon
     */
    public function __construct($lat, $lon)
    {
        $this->lat = $lat;
        $this->lon = $lon;
    }


    /**
     * @param Location $location
     * @return float
     */
    public function distanceFrom(Location $location)
    {
        return (
        rad2deg(
            acos((
                    sin(deg2rad($this->getLat())) *
                    sin(deg2rad($location->getLat()))
                ) + (
                    cos(deg2rad($this->getLat())) *
                    cos(deg2rad($location->getLat())) *
                    cos(deg2rad($this->getLon() - $location->getLon())))
            )
        )
        ) * 60 * 1.1515;
    }

    /**
     * @return mixed
     */
    function getLat()
    {
        return (float)$this->lat;
    }

    /**
     * @param float $lat
     */
    function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    function getLon()
    {
        return (float)$this->lon;
    }

    /**
     * @param float $lon
     */
    function setLon($lon)
    {
        $this->lon = $lon;
    }

}
