<?php


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
define('APP_PATH', realpath(''));

try {
    include APP_PATH . "/app/core/autoloder.php";

    include APP_PATH . "/app/fatController.php";

} catch (\Exception $e) {
    echo $e->getMessage();
}
