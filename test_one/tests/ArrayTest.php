<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 22/02/2016
 * Time: 10:26
 */

namespace tests;


class ArrayTest extends \PHPUnit_Framework_TestCase
{

    public function testFlattenArray()
    {
        $testArr = [[1,2,[3]],4];
        $expectedArr =  [1,2,3,4];
        $test = flattenArray($testArr);
        $result=array_diff($expectedArr,$test);
        $this->assertTrue(empty($result));
    }
    public function testFlattenArray2()
    {
        $range = range(1,100);

        $test = flattenArray(funkyArray(1,100));
        $result=array_diff($range,$test);
        $this->assertTrue(empty($result));
    }

}