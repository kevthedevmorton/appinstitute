<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 22/02/2016
 * Time: 11:21
 */
class RecursiveArrayOnlyIterator extends RecursiveArrayIterator
{
    public function hasChildren()
    {
        // fixes problems if current array is object
        return (bool) @is_array($this->current());
    }
}