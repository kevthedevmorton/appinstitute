<?php
require_once('RecursiveArrayOnlyIterator.php');


/**
 * @param array $array
 * @param bool $order
 * @return array
 */
function flattenArray(array $array , $order = true){
    $r = [];
    // This is the 1st result form stack with one difference,  RecursiveArrayIterator this will fail if any objects are in array this will not.
    $ittaratebuls = new RecursiveIteratorIterator(new RecursiveArrayOnlyIterator($array));
    foreach($ittaratebuls as $value) {
        array_push($r,$value);
    }
    if($order){
        sort($r, SORT_NUMERIC);
    }
    return $r;
}

/**
 * @param int $from
 * @param int $to
 * @param array $chunks
 * @return array
 */
function funkyArray($from = 0 , $to = 100 , array $chunks = null){
    // this is a good one Kev is this P or NP ? it could  iterate forever but can we calculate when it will stop iterating?
    if (empty($chunks)){
        $chunks = array_chunk(range($from,$to) , mt_rand($from,$to));
    }
    foreach($chunks as $key => $chunk){
        if((bool) mt_rand(0,1) && is_array($chunk)){
            $chunks[$key] = funkyArray($from = 0 , $to = 100 ,$chunks );
            shuffle($chunks[$key]);
        }

    }
    return $chunks;
}


